<?php
session_start();
if (isset($_SESSION['user'])) { ?>

<html>
<head>
	<meta charset="utf-8" />
    <link href="css/bootstrap.css" rel="stylesheet" />
    <link href="css/bootstrap-responsive.css" rel="stylesheet" />
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
	
<link rel="stylesheet" type="text/css" href="style.css">
</head>   
<body>
 <meta charset="utf-8" />
 
<div class="container" style="width:50%; margin-top:20px">
            <div class="navbar navbar-inverse">
    <div class="navbar-inner">
    <div class="container">
     
    <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    </a>
     
    <!-- Be sure to leave the brand out there if you want it shown -->
    <a class="brand" class="active" href="#">Головна</a>
     
        <ul class="nav">
    <li><a href="teachers.php">Викладачі</a></li>
	<li><a href="seminars.php">Семінари</a></li>
	<li><a href="schedule.php">Розклад</a></li>
	<li><a href="logout.php">Вийти</a></li>
    </ul>
    <div class="nav-collapse collapse">
    <!-- .nav, .navbar-search, .navbar-form, etc -->
    </div>
     
    </div>
    </div>
    </div>

	<p class="lead">У вас немає прав для виконання даної дії</p>
    
</div>

</body>
</html>

<?php } else { 
	echo "LOL?"; 
} ?>