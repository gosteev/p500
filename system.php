<?php
session_start();
if (isset($_SESSION['user'])) { ?>


<html>
<head>
	<meta charset="utf-8" />
    <link href="css/bootstrap.css" rel="stylesheet" />
    <link href="css/bootstrap-responsive.css" rel="stylesheet" />
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>

	<link rel="stylesheet" type="text/css" href="style.css">
</head>   
<body>
 <meta charset="utf-8" />
 
<div class="container" style="width:50%; margin-top:20px">
            <div class="navbar navbar-inverse">
    <div class="navbar-inner">
    <div class="container">
     
	 
    <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    </a>
     
    <!-- Be sure to leave the brand out there if you want it shown -->
    <a class="brand" class="active" href="#">Головна</a>
     
        <ul class="nav">
    <li><a href="teachers.php">Викладачі</a></li>
	<li><a href="seminars.php">Семінари</a></li>
	<li><a href="schedule.php">Розклад</a></li>
	<li><a href="logout.php">Вийти</a></li>
    </ul>
    <div class="nav-collapse collapse">
    <!-- .nav, .navbar-search, .navbar-form, etc -->
    </div>
     
    </div>
    </div>
    </div>

	<?php
	
	if ($_SESSION['admin']=='true')
		echo('<p class="text-right" style="color:red; float:right">Role: Administrator.</p>');
	else if ($_SESSION['admin']=='false')
		echo('<p class="text-right" style="color:blue; float:right">Role: User.</p>');
	?>	
	 
	
    <a href="#addModal" role="button"  class="btn btn-primary btn-large" data-toggle="modal">Додати викладача</a>
     
    <!-- Modal -->
    <div id="addModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
    <form method="post" action="add.php">
    <fieldset>
    <legend>Додати викладача</legend>

    <input type="text" placeholder="Ім'я" required="true" name="name"> &nbsp;&nbsp;
	<input type="text" placeholder="Логін для входу" required="true" name="login">
	<br>
    <input type="text" placeholder="Посада" required="true" name="post"> &nbsp;&nbsp;
	<input type="password" placeholder="Пароль для входу" required="true" name="pass">
	<br>
    <input type="text" placeholder="Наукова степінь" required="true" name="degree"><br>
    <input type="text" placeholder="Телефон" name="phone"><br>
	<input type="text" placeholder="email" name="email"><br>
	<input type="text" placeholder="photo link" name="photo">
	<br>
		
		<button class="btn btn-success">Додати</button>
		<button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Закрити</button>
    </fieldset>
    </form>
    </div>
    </div>
					
	<br><br>
	
	<a href="#deleteModal" role="button"  class="btn btn-primary btn-large" data-toggle="modal">Видалити викладача</a>
     
    <!-- Modal -->
    <div id="deleteModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
    <fieldset>
    <legend>Видалити викладача</legend>
    
    Виберіть викладача <br>

    <form method="post" action="delete.php">
    <?php
		require_once'db.php';
		
		$result = mysql_query("SELECT name FROM teachers");
		$i=0;
		while ($row = mysql_fetch_assoc($result)) {
			$name[$i] = $row['name'];
			$i++;
		}
		echo('<select name="author">');
		for ($i=0;$i<count($name);$i++){
			echo('<option value="'.$name[$i].'">'.$name[$i].'</option>');
		}
		echo("</select>");
		//print_r($_SESSION);
		
		?>
		
		<br>
		<button type="submit" class="btn btn-danger">Видалити</button> &nbsp;
		<button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Закрити</button>
    </form>
    </div>
    </div>
	
</div>

</body>
</html>

<?php } else { 
	echo "LOL?"; 
} ?>